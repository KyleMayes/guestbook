// Copyright 2017 Kyle Mayes
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![feature(custom_derive)]
#![feature(plugin)]

#![plugin(rocket_codegen)]

extern crate chrono;
#[macro_use]
extern crate lazy_static;
extern crate rocket;
extern crate rocket_contrib;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

use std::collections::{BTreeMap};
use std::path::{Path, PathBuf};
use std::sync::{RwLock};

use chrono::{NaiveDate, UTC};

use rocket::http::uri::{URI};
use rocket::request::{Form};
use rocket::response::{NamedFile, Redirect};
use rocket_contrib::{Template};

fn now() -> NaiveDate {
    UTC::now().naive_utc().date()
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct Comment {
    name: String,
    date: NaiveDate,
    message: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct Entry {
    name: String,
    date: NaiveDate,
    comments: Vec<Comment>,
}

lazy_static! { static ref ENTRIES: RwLock<Vec<Entry>> = RwLock::new(vec![]); }

// "/" -------------------------------------------

#[get("/")]
fn index() -> Template {
    let mut map = BTreeMap::new();
    map.insert("entries", (*ENTRIES.read().unwrap()).clone());
    Template::render("index", &map)
}

// "/error" --------------------------------------

#[derive(FromForm)]
struct ErrorParams {
    message: String,
}

#[get("/error?<params>")]
fn error(params: ErrorParams) -> Template {
    let mut map = BTreeMap::new();
    map.insert("message", params.message.clone());
    Template::render("error", &map)
}

fn report(message: &str) -> Redirect {
    Redirect::to(&format!("/error?message={}", URI::percent_encode(message)))
}

// "/<path..>" -----------------------------------

#[get("/<path..>")]
fn files(path: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("files").join(path)).ok()
}

// "/sign-in" ------------------------------------

#[derive(FromForm)]
struct SignInForm {
    name: String,
}

#[post("/sign-in", data="<form>")]
fn sign_in(form: Form<SignInForm>) -> Redirect {
    let form = form.get();

    // Check for a non-empty name.
    if form.name.is_empty() {
        return report("Missing sign-in name.");
    }

    // Add the entry.
    let entry = Entry { name: form.name.clone(), date: now(), comments: vec![] };
    (*ENTRIES.write().unwrap()).push(entry);
    Redirect::to("/")
}

// "/comment" ------------------------------------

#[derive(FromForm)]
struct CommentForm {
    index: usize,
    name: String,
    message: String,
}

#[post("/comment", data="<form>")]
fn comment(form: Form<CommentForm>) -> Redirect {
    let form = form.get();

    // Check for a non-empty name.
    if form.name.is_empty() {
        return report("Missing comment name.");
    }

    // Check for a non-empty message.
    if form.message.is_empty() {
        return report("Missing comment message.");
    }

    // Add the comment.
    let comment = Comment { name: form.name.clone(), message: form.message.clone(), date: now() };
    (*ENTRIES.write().unwrap())[form.index].comments.push(comment);
    Redirect::to("/")
}

// "/_ah/health" ---------------------------------

#[get("/_ah/health")]
fn ah_health() -> String {
    "OK".into()
}

fn main() {
    rocket::ignite().mount("/", routes![index, error, files, sign_in, comment, ah_health]).launch();
}
